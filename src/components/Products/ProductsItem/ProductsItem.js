import React from 'react';
import classes from './ProductsItem.sass';
import imageProd from './img.svg';
import Button from '../../UI/Button/Button';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {addProductInBasket} from "../../../redux/actions/Showcase";

const ProductsItem = (props) => {

  const cls = [
    classes.ProductsItem,
    props.className
  ];

  return (
      <div className={cls.join(' ')} style={{color: props.product.description, margin: '10px'}}>
        <h3>{props.product.name}</h3>
        <img src={imageProd} alt={props.product.cover}/>
        <p className={classes.price}>Price: {props.product.price}</p>
        <Button
            type={'primary'}
            onClick={() => props.history.push('/' + props.product.id.toLowerCase())}
        >
          Смотреть товар
        </Button>
        <Button
            type={'success'}
            onClick={() => props.addProductInBasket({
              id: props.product.id,
              name: props.product.name,
              price: props.product.price,
              cover: props.product.cover,
              description: props.product.description,
              count: 1})}
        >
          Добавить в корзину
        </Button>
      </div>
  )
};

function mapStateToProps(state) {
  return {
    basketElements: state.Showcase.basketElements,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    addProductInBasket: product => dispatch(addProductInBasket(product)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductsItem))