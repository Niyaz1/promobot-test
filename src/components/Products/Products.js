import React from 'react';
import classes from './Products.sass';
import ProductsItem from './ProductsItem/ProductsItem';
import Slider from "react-slick";

const Products = (props) => {

  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "red", borderRadius: 50 }}
            onClick={onClick}
        />
    );
  }
  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "green", borderRadius: 50 }}
            onClick={onClick}
        />
    );
  }

  let countItems;
  if (props.countElements > 3) {
    countItems = 3;
  }
  if (props.countElements < 3 && props.countElements > 1) {
    countItems = 2;
  }
  if (props.countElements < 2) {
    countItems = 1;
  }

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: countItems,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />
  };


  return (
      <React.Fragment>
        <Slider {...settings} className={classes.Products}>
          {
            props.products.map((product, index) => {
              if (index < props.countElements) {
                return (
                    <div key={index} style={{margin: 10}}>
                      <ProductsItem
                          product={product}
                          index={index}
                      />
                    </div>
                )
              }
              else return null
            })
          }
        </Slider>
      </React.Fragment>
  )
};

export default Products