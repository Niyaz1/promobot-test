import React from "react";
import classes from './Button.sass'

const Button = props => {
		const cls = [
			props.className,
			classes.Button,
			classes[props.type]
		];

		return (
				<button
					onClick={props.onClick}
					className={cls.join(' ')}
					disabled={props.disabled}
				>
					{props.children}
				</button>
		)
};

export default Button