import React, {Component} from 'react';
import classes from './ProductDetail.sass';
import imageProd from '../../components/Products/ProductsItem/img.svg';
import Button from "../../components/UI/Button/Button";
import {connect} from 'react-redux';
import {addProductInBasket} from "../../redux/actions/Showcase";

class ProductDetail extends Component {

	render() {
		const productID = this.props.match.params.id;
		const product = this.props.products.filter(product => product.id === productID);

		return (
				<React.Fragment>
					<div className={classes.ProductPage} style={{color: product[0].description}}>
						<h2>{product[0].name}</h2>
						<figure>
							<img src={imageProd} alt={product[0].cover}/>
							<figcaption>{product[0].cover}</figcaption>
						</figure>
						<div className={classes.description}>
							<p>{product[0].price}</p>
							<p>{product[0].description}</p>
							<Button type={'success'} onClick={() => this.props.addProductInBasket({
								id: product[0].id,
								name: product[0].name,
								price: product[0].price,
								cover: product[0].cover,
								description: product[0].description,
								count: 1})}>
								Добавить в корзину
							</Button>
						</div>
					</div>
				</React.Fragment>
		)
	};
}

function mapStateToProps(state) {
	return {
		products: state.Showcase.products
	}
}

function mapDispatchToProps(dispatch) {
	return {
		addProductInBasket: product => dispatch(addProductInBasket(product)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail)