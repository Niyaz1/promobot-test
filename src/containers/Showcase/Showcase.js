import React, {Component} from 'react';
import {connect} from 'react-redux';
import classes from './Showcase.sass';
import Products from '../../components/Products/Products';
import {fetchProducts} from "../../redux/actions/Showcase";

class Showcase extends Component {

  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    return(
      <div className={classes.Showcase}>
        <Products
          products={this.props.products}
          countElements={this.props.showElements}
        />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    showElements: state.Showcase.showElements,
    products: state.Showcase.products,
    basketElements: state.Showcase.basketElements
  }
}

function mapDispatchToProps(dispatch) {
  return {
     fetchProducts: () => dispatch(fetchProducts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Showcase)