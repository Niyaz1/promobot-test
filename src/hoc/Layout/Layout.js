import React, {Component} from 'react';
import classes from './Layout.sass';
import {Route, Switch} from 'react-router-dom';
import Showcase from '../../containers/Showcase/Showcase';
import ProductDetail from "../../containers/ProductDetail/ProductDetail";
import Header from "../../components/Header/Header";

class Layout extends Component {

  state = {
    title: 'Витрина'
  };

  render() {
    return (
      <div className={classes.Layout}>
        <Header title={this.state.title} className={classes.Header}/>
        <Switch>
          <Route path="/:id" component={ProductDetail} />
          <Route path="/"  component={Showcase} />
        </Switch>
      </div>
    )
  }
}

export default Layout